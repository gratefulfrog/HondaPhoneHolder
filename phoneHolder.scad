$fn = 100;

dxfFileName     = "DXF/phoneHolder_02.dxf";
holderLayer     =  "HolderProfile";
angleBlockLayer =  "AngleBlock";

m3D         = 3.4;
m3CD        = 5.9;
m3H         = 10;
m3W         = 5.5+0.2;
m3WD        = m3W/cos(30);
m3NH        = 2.7;  //
m3NHRounded = 3;
m3L         = 10;

wallThickness = 3;

pinOffset =  5.5;
pinDia    =  6.2;
testZ     = 24; //2*pinOffset + pinDia;

module pin(){
    rotate([0,0,-30])
    rotate([90,0,0])
    cylinder(h=7,d=pinDia,center= true);
  }
  //pin();
module holderScrewHole(i){
  nutH =  i==0 ?18:10;
  sH   =  20;
  translate([(baseR+postDia/2.)/2.,0,-wallThickness])
    union(){
      translate([0,0,m3L-m3NHRounded])
        cylinder(d=m3CD,h=20);
      cylinder(d=3.4,h= sH ); //, center=true);
      translate([0,0,-nutH])
        cylinder(d=m3WD,h= nutH,$fn=6, center=false);
    }
}
//holderScrewHole(1);

module holderScrewHoleX(n){
  for (i=[0:360/n:360*(1-1/n)])
    rotate([0,0,360/2*n + i])
      holderScrewHole(i);
}
//holderScrewHoleX(3);

module horizontalScrewHoleX(n){
  rotate([0,-90,0])
    rotate([0,0,90])
      translate([(baseR+postDia/2.)/2.,0,0])
      holderScrewHoleX(n);
}
//%horizontalScrewHoleX(3);
//%linear_extrude(testZ,center=true)
  //      import(dxfFileName,layer=holderLayer);

module testHolder(){
  difference(){
    union(){
      linear_extrude(testZ,center=true)
        import(dxfFileName,layer=holderLayer);
    }
    union(){
      horizontalScrewHoleX(3);
    }
  }
}
//testHolder();

///// POST /////////////  
  
  
  sphereDia  = 19;
  postHeight = 15 + sphereDia/2.;
  postDia    = 9;
  module post(){
    translate([0,0,postHeight])
      sphere(d=sphereDia);
    cylinder(h=postHeight,d=postDia);
  }
  wingX = 6;
  wingY = 12;
  module wing(){
    pts = [[0,0],[0,wingY],[wingX,0]];  
    translate([postDia/2.-0.2,0,0])
      rotate([90,0,0])
        linear_extrude(1.5,center=true)
          polygon(pts);
  }
  module wingsX(n){
    for (i=[0:360/n:360*(1-1/n)])
      rotate([0,0,i])
        wing();
  }
  
  baseHeight = 3;
  baseR      = wingX+postDia/2.;
  echo("base radius: ",baseR);
  module baseDisk(){
    translate([0,0,-baseHeight])
      cylinder(h=baseHeight,r=baseR);
  }
  
  
  module threadCyl(){
    translate([0,0,-5])
      cylinder(h=50,d=m3D);
  }
  module capCyl(){
    translate([0,0,m3H])
      cylinder(h=50,d=m3CD);
  }
echo("screw offseft: ", (baseR+postDia/2.)/2.);
module screwHole(hh=40){
  translate([(baseR+postDia/2.)/2.,0,0])
    cylinder(d=m3D,h=hh,center=true);
}
module screwHoleX(n,hh=40){
  for (i=[0:360/n:360*(1-1/n)])
    rotate([0,0,360/2*n +i])
      screwHole(hh);
}

module positivePart(n){
   union(){
    baseDisk();
    wingsX(n);
    post();
   }
 }  

module negativePart(n){
  union(){
    screwHoleX(n,20);
   }
 }
 module fullPost(){
   difference(){
     positivePart(3);
     negativePart(3);
   }
 }
//fullPost();
 
//////////////// Angle Block /////
 
module angleHoles(){ 
  screwHoleX(3);
  rotate([0,30,0])
    translate([5,0,10])
      rotate([0,0,60])
        holderScrewHoleX(3); 
}

module angleBlock(production=false){
  rotY  = production ? 90 : 0;
  tradX = production ? -50 : 0;
  translate([tradX,0,0])
  rotate([0,rotY,0])
  difference(){
    union(){
      linear_extrude(testZ,center=true)
        import(dxfFileName,layer=angleBlockLayer);
    }
    union(){
      horizontalScrewHoleX(3);
      translate([-15,15,0])
        rotate([0,0,-30]) 
          horizontalScrewHoleX(3);
    }
  }
}

translate([0,70,0]){
  angleBlock();
  testHolder();
}
translate([70,0,0])
  fullPost();

angleBlock(true);
testHolder();
